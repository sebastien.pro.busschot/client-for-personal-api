//npm install xmlhttprequest


function myIp(){
    var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
    const req = new XMLHttpRequest();
    req.open('GET', 'https://api.myip.com', false); 
    req.send(null);
    let res = req.responseText;
    let ip = JSON.parse(res).ip;

    return ip;
}

function sendMyIp(ip){
    var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
    const req = new XMLHttpRequest();
    req.open('POST', 'http://busschot.fr/ipup', true);

    req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    req.send("ip="+ip);

    //console.log(req);
}

sendMyIp(myIp());